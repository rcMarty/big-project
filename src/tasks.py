from typing import Dict, List


def fizzbuzz(num: int) -> str:
    """
    Return 'Fizz' if `num` is divisible by 3, 'Buzz' if `num` is divisible by 5, 'FizzBuzz' if `num`
    is divisible both by 3 and 5.
    If `num` isn't divisible neither by 3 nor by 5, return `num`.
    Example:
        fizzbuzz(3) # Fizz
        fizzbuzz(5) # Buzz
        fizzbuzz(15) # FizzBuzz
        fizzbuzz(8) # 8
    """
    if num % 3 == 0 and num % 5 == 0:
        return "FizzBuzz"
    elif num % 3 == 0:
        return "Fizz"
    elif num % 5 == 0:
        return "Buzz"
    else:
        return num

def fibonacci(n: int) -> int:
    """
    Return the `n`-th Fibonacci number (counting from 0).
    Example:
        fibonacci(0) == 0
        fibonacci(1) == 1
        fibonacci(2) == 1
        fibonacci(3) == 2
        fibonacci(4) == 3
    """
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibonacci(n-1) + fibonacci(n-2)


def dot_product(a: List[int], b: List[int]) -> int:
    """
    Calculate the dot product of `a` and `b`.
    Assume that `a` and `b` have same length.
    Hint:
        lookup `zip` function
    Example:
        dot_product([1, 2, 3], [0, 3, 4]) == 1 * 0 + 2 * 3 + 3 * 4 == 18
    """
    result = 0
    for x, y in zip(a, b):
        result += x * y
    return result


def redact(data: str, chars: str) -> str:
    """
    Return `data` with all characters from `chars` replaced by the character 'x'.
    Characters are case sensitive.
    Example:
        redact("Hello world!", "lo")        # Hexxx wxrxd!
        redact("Secret message", "mse")     # Sxcrxt xxxxagx
    """
    redacted = ""
    for char in chars:
        data = data.replace(char, 'x')
    return data


def count_words(data: str) -> Dict[str, int]:
    """
    Return a dictionary that maps word -> number of occurences in `data`.
    Words are separated by spaces (' ').
    Characters are case sensitive.

    Hint:
        "hi there".split(" ") -> ["hi", "there"]

    Example:
        count_words('this car is my favourite what car is this')
        {
            'this': 2,
            'car': 2,
            'is': 2,
            'my': 1,
            'favourite': 1,
            'what': 1
        }
    """
    dictionary: dict = {}
    if len(data) == 0:
        return dictionary
    for word in data.split(" "):
        dictionary.update({word: dictionary.get(word, 0) + 1})
    return dictionary


def bonus_fizzbuzz(num: int) -> str:
    """
    Implement the `fizzbuzz` function.
    `if` and cycles are not allowed.
    """

    return (num % 3 == 0 and num % 5 == 0) and "FizzBuzz" or (num % 3 == 0) and "Fizz" or (num % 5 == 0) and "Buzz" or num


def hex_print(list: List[int]):
    for item in list:
        print(hex(item))

def bin_print(list: List[int]):
    for item in list:
        print(bin(item))

def bonus_utf8(cp: int) -> List[int]:
    """
    Encode `cp` (a Unicode code point) into 1-4 UTF-8 bytes - you should know this from `Základy číslicových systémů (ZDS)`.
    Example:
        bonus_utf8(0x01) == [0x01]
        bonus_utf8(0x1F601) == [0xF0, 0x9F, 0x98, 0x81]
    """
    '''
    mask = 1
    binary: List[int] = []
    head: List[int] = []
    body: List[List[int]] = []

    print(bin(cp))

    temp = []
    num = 0

    for i in range(23):
        print(i)
        if ((i) % 6 == 0 and i != 0 or i == 22):
            print("appending", i)
            body.append(temp)
            head.append(num)
            num = 0
            temp = []
            cp = cp>>6
            mask = mask>>6
        temp.append((cp & mask) > 0 and 1 or 0)
        num |= (cp & mask)
        mask = mask<<1

    # 4 bytes
    if head[2] != 0:
        print("4 bytes")
    elif head[1] != 0:
        print("3 bytes")


    print(body)
    bin_print(head)

    byteCount = len(body)
    for i in range(byteCount - 1):
        body[i].append(0)
        body[i].append(1)

    print(body)

    for i in range(4):
        if (i + byteCount) >= 4:
            body[byteCount-1].append(1)
            print("appending 1,", i)
        else:
            print("appending 0,", i)
            body[byteCount-1].append(0)

    for word in body:
        temp = 0


    print(body)

    return binary'''

    # nahore muj vlastni pokus se sesitem ze zds (ktery jaksi nefunguje)
    # dole za pomoci wiki

    if cp < 0x7F:
        return [cp]
    elif cp < 0x7FF:
        return [0b11000000 | (cp >> 6) & 0b111111, 0b10000000 | (cp) & 0b111111]
    elif cp < 0xFFFF:
        return [0b11100000 | (cp >> 12) & 0b111111, 0b10000000 | (cp >> 6) & 0b111111, 0b10000000 | (cp) & 0b111111]
    else:
        return [0b11110000 | (cp >> 18) & 0b111111, 0b10000000 | (cp >> 12) & 0b111111, 0b10000000 | (cp >> 6) & 0b111111, 0b10000000 | (cp) & 0b111111]

print("a")