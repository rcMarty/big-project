# "Velká" práce

## Autoři

- Martin Korotwitschka (KOR0289)
- Barbora Kovalská (KOV0354)

## Závěrečná práce

Závěrečná práce vychází z "malé" práce. Použijte svůj projekt, na kterém pracujete a dejte jej do GIT repozitáře a pokuste se připravit kontinuální integraci svého projektu. V závislosti na použitém programovacím jazyce či sadě programovacích jazyků navhrněte a vytvořte vhodné úkoly ve vaší kontinuální integraci. Pro úlohy v CI použijte vhodné Docker kontejnery, případně vytvořte vlastní. Pokud povaha CI vyžaduje, rozdělte úlohy (jobs) do vhodných fází (stages).

Požadavky na kvalitu práce (hodnotící kritéria):
* [x] GIT repozitář na Gitlabu.
  * [x] Repozitář má README.md (K čemu projekt slouží, jak se používá, jak se instaluje, jaké jsou prerekvizity, apod.)
* [x] Použití vlastního projektu (ideální kandidát má kombinaci jazyků - např. Python a BASH, Markdown a JS, apod.)
* [x] Vytvořená CI v repozitáři.
* [x] CI má minimálně tři úlohy:
  * [x] Test kvality Markdown stránek. (Kvalita dokumentace je důležitá. Minimálně README.md by mělo být v repozitáři dostupné.)
  * [x] Kontrola syntaxe každého jazyka.
  * [x] Použití "lint" nástroje pro každý jazyk.
* [ ] (Volitelné/doporučené) Nástroj na kontrolu dostupnosti aktualizací/security updates pro moduly jazyka (pokud existuje).
* [x] (Volitelné/doporučené) Nástroj na kontrolu testů/code coverage (pokud existuje).
* [x] (Volitelné/doporučené) Pokud některé nástroje umí generovat HTML reporty - umistěte je na Gitlab pages.
* [ ] (Volitelné/doporučené) Repozitář obsahuje šablonu ticketu.

# Instalace

- Vytvořte virtual environment: `python3 -m venv venv`
- Aktivujte venv: `source venv/bin/activate`
- Nainstalujte requirements.txt: `pip install -r requirements.txt`

# Použití

## Program

- Spuštění programu: `python source.py`
- Spuštění testů: `python -m pytest -v test.py`

## Dokumentace

- Dostaňte se do složky `docs`
- Vygenerujte dokumentaci: `mkdocs serve`
